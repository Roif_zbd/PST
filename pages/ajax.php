<?php 
	session_start();
	include "../config/config.php";
	include "../config/database.php";
	include "../".$config["webspira"]."config/connection.php";


	$conn_header = new mysqli($mysql["server"], $mysql["username"], $mysql["password"], $mysql["database"]);
	if ($conn_header->connect_error) { die("Connection failed: " . $conn_header->connect_error); }

	if($_POST["tipe"] == "setPrintData"){
		$_SESSION["print"]["company_mode"]=$_POST["company_mode"];
		$_SESSION["print"]["company_logo"]=$_POST["company_logo"];
		$_SESSION["print"]["company_name"]=$_POST["company_name"];
		$_SESSION["print"]["company_address"]=$_POST["company_address"];
		$_SESSION["print"]["company_contact"]=$_POST["company_contact"];
		$_SESSION["print"]["title"]=$_POST["title"];
		$_SESSION["print"]["periode"]=$_POST["periode"];
		$_SESSION["print"]["header"]=$_POST["header"];
		$_SESSION["print"]["table_header"]=$_POST["table_header"];
		$_SESSION["print"]["table_body"]=$_POST["table_body"];
		$_SESSION["print"]["table_footer"]=$_POST["table_footer"];
		$_SESSION["print"]["total_column"]=$_POST["total_column"];
	}

	if($_POST["tipe"] == "deleteGeneratespp"){
		$array = array();
		$nomor = $_POST["nomor"];
		$user = $_SESSION["login"]["nomor"];
		$header = $_POST["header"];

		$sql = "CALL sp_delete_generatespp(".$nomor.",".$user.",".$header.");";
		$result = $conn_header->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		array_push($array, $row);
	        }
	    }
		echo json_encode($array);
	}

	if($_POST["tipe"] == "deleteDps"){
		$array = array();
		$nomor = $_POST["nomor"];
		$user = $_SESSION["login"]["nomor"];
		$header = $_POST["header"];

		$sql = "CALL sp_delete_dps(".$nomor.",".$user.",".$header.");";
		$result = $conn_header->query($sql);
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
        		array_push($array, $row);
	        }
	    }
		echo json_encode($array);
	}

	if($_POST["tipe"] == "waktu_uang_masuk"){

		$nama_file = $_POST["nama"];
		$tipe_um = $_POST["tipe_um"];
		$jenis = $_POST["jenis"];

		$data = array();

		$sql = "SELECT DISTINCT waktu_upload 
		FROM thuangmasuk 
		WHERE status_aktif = 1 
		AND nama_file = '".$nama_file."'
		AND (waktu_upload IS NOT NULL AND waktu_upload <> '') 
		AND tipe = ".$tipe_um." AND transaksi_jenis LIKE '".$jenis."'";

		$result = $conn_header->query($sql);
		$i = 0;
		while ($row = $result->fetch_array()) {
			$data[$i++] = [
				"waktu_upload"=>$row["waktu_upload"]
			];
		}
		echo json_encode($data);
	}
	
	if($_POST["tipe"] == "jadwal_bentrok"){
		$array = array();
		$nomor = $_POST["nomor"];
		$nomor_jemaat = $_POST["nomormhjemaat"];
		$tanggal = $_POST["tanggal"];
		$jam_mulai = $_POST["jam_mulai"];
		$jam_selesai = $_POST["jam_selesai"];
		$user = $_SESSION["login"]["nomor"];
		$header = $_POST["header"];

		$sql = "SELECT
		a.*,
		b. nama_lengkap,
		c. tanggal,
		c. jam_mulai,
		c. jam_selesai
	  FROM
		tdjadwalkegiatan AS a
		JOIN mhjemaat AS b
		ON a.nomormhjemaat = b.nomor
		JOIN thjadwalkegiatan AS c
		ON a.nomorthjadwalkegiatan = c.nomor
		WHERE a.nomormhjemaat = $nomor_jemaat
		AND (CONCAT (c.tanggal, ' ', c.jam_mulai) BETWEEN '".$tanggal." ".$jam_mulai."' AND '".$tanggal." ".$jam_selesai."'
  OR CONCAT (c.tanggal, ' ', c.jam_selesai) BETWEEN '".$tanggal." ".$jam_mulai."' AND '".$tanggal." ".$jam_selesai."')";
		$result = $conn_header->query($sql);
		if ($result->num_rows > 0) {
			echo(false);
	    } else {
			echo(true);
		}
	}
?>