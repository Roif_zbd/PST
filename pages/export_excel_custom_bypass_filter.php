<?php
session_start();
include "../config/config.php";
include "../config/database.php";

$title  = $_POST["export_title"];
$export_column = $_POST["export_column"];
$query  = $_POST["export_query"];

// Create connection
$mysqli = new mysqli($mysql["server"], $mysql["username"], $mysql["password"], $mysql["database"]);

function clean_data($str)
{
	// escape tab characters
	$str = preg_replace("/\t/"," ",$str);
	// escape new lines
	$str = preg_replace("/\r?\n/"," ",$str);
	// escape fields that include double quotes
	if(strstr($str,'"'))
		$str = '"'.str_replace('"','""',$str).'"';
	// escape new lines in html
	$str = str_replace("&nbsp;"," ",$str);
	$str = preg_replace("/\<br>/"," ",$str);
	return $str;
}

// ** START DECLARE VARIABLE
$rows 				= array();
$rowstyle 			= array();
$new_array 			= array();
// ** END DECLARE VARIABLE

// ** START CREATE HEADER TABLE
$header = array();

// ** START CREATE HEADER TABLE
foreach ($export_column as $col) {
	$new_col = explode("~",$col);
	if(isset($new_col[1])){
		$header[$new_col[0]] = $new_col[1];
	}else{
		$header[$new_col[0]] = "string";
	}
}
  
$i=0;
foreach ($export_column as $col) {
	$new_col = explode("~",$col);
	$index_table_export["column"][$i]["name"] = $new_col[0];
	$i++;
}

$column = $index_table_export["column"];
$result = $mysqli->query($query);
 //var_dump($query);

// ======================================================================================================================================================== //
// ** START RENDER TABLE
include_once("PHP_XLSXWriter-master/xlsxwriter.class.php");
ini_set('display_errors', 0);
ini_set('log_errors', 1);
error_reporting(E_ALL & ~E_NOTICE);

date_default_timezone_set("Asia/Bangkok");
$filename = $title.".xlsx";
header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
header("Content-Type: application/xlsx");
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate');
header('Pragma: public');

$writer = new XLSXWriter();
$writer->setAuthor('Some Author');

$styles0 = array( 'fill'=>'transparent' );
$styles1 = array( 'font-style'=>'bold' );
$styles2 = array( 'font-style'=>'bold', 'halign'=>'center' );

$Sheet1 = "Sheet1";

$writer->writeSheetHeader($Sheet1, $header);
while ($data = $result->fetch_assoc()) {
	$new_array = array();

	// ** DATA PEGAWAI
	foreach($column as $col)
	{
		array_push($new_array, clean_data($data[$col["name"]]));
	}

	// ** PUSH ARRAY
	$writer->writeSheetRow($Sheet1, $new_array);
}

$writer->writeToStdOut();

?>