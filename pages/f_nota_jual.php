<?php 
session_start();
include "../config/config.php";
include "../config/database.php";
include "../".$config["webspira"]."config/connection.php";

$nomor = $_POST["nomor"];
$tanggal = $_POST["tanggal"];
$datas = array();
$query = "  
          SELECT
            'JUAL_NOTA' AS jenis,
            '0' AS tipe,
            '1' AS multiplier,
            a.kode AS transaksi_kode,
            a.tanggal AS transaksi_tanggal,
            a.faktur_pajak_nomor,
            (a.total_valuta-a.total_terbayar) AS sisa,
            (a.total_valuta-a.total_terbayar)*1 AS subtotal,
            0 AS nol,
            a.keterangan AS keterangan,
            'thjualnota' AS transaksi_tabel,
            a.nomor AS transaksi_nomor,
            '' AS kosong,
            1 AS nomormhaccount
          FROM thjualnota a
          JOIN mhrelasi b ON a.nomormhrelasi = b.nomor 
          WHERE a.status_disetujui = 1
          AND (a.total_valuta-a.total_terbayar) > 0
          AND a.nomormhcabang = ".$_SESSION["cabang"]["nomor"]."
          AND a.tipe = 4
          AND a.tanggal <= STR_TO_DATE('".$tanggal."', '".$_SESSION["setting"]["date_sql"]."') 
          AND FIND_IN_SET(a.nomor, '".$nomor."')
          UNION ALL
          SELECT
            'JUAL_NOTA' AS jenis,
            '0' AS tipe,
            '1' AS multiplier,
            a.kode AS transaksi_kode,
            a.tanggal AS transaksi_tanggal,
            a.faktur_pajak_nomor,
            (a.total_valuta-a.total_terbayar) AS sisa,
            (a.total_valuta-a.total_terbayar)*1 AS subtotal,
            0 AS nol,
            a.keterangan AS keterangan,
            'thjualnota' AS transaksi_tabel,
            a.nomor AS transaksi_nomor,
            '' AS kosong,
            1 AS nomormhaccount
          FROM thjualnota a
          JOIN mhrelasi b ON a.nomormhrelasi = b.nomor 
          WHERE a.status_disetujui = 1
          AND (a.total_valuta-a.total_terbayar) > 0
          AND a.nomormhcabang = ".$_SESSION["cabang"]["nomor"]."
          AND (a.tipe = 1 OR a.tipe = 0)
          AND a.tanggal <= STR_TO_DATE('".$tanggal."', '".$_SESSION["setting"]["date_sql"]."') 
          AND FIND_IN_SET(a.nomor, '".$nomor."')
          UNION ALL
          SELECT
            'NOTA_ASET' AS jenis,
            '0' AS tipe,
            '1' AS multiplier,
            a.kode AS transaksi_kode,
            a.tanggal AS transaksi_tanggal,
            a.faktur_pajak_nomor,
            (a.total_valuta-a.total_terbayar) AS sisa,
            (a.total_valuta-a.total_terbayar)*1 AS subtotal,
            0 AS nol,
            a.keterangan AS keterangan,
            'thjualnota' AS transaksi_tabel,
            a.nomor AS transaksi_nomor,
            '' AS kosong,
            1 AS nomormhaccount
          FROM thjualnota a
          JOIN mhrelasi b ON a.nomormhrelasi = b.nomor 
          WHERE a.status_disetujui = 1
          AND (a.total_valuta-a.total_terbayar) > 0
          AND a.nomormhcabang = ".$_SESSION["cabang"]["nomor"]."
          AND a.tipe = 2
          AND a.tanggal <= STR_TO_DATE('".$tanggal."', '".$_SESSION["setting"]["date_sql"]."') 
          AND FIND_IN_SET(a.nomor, '".$nomor."')
          ";
$mysqli_query = mysqli_query($con, $query);

while($r = mysqli_fetch_array($mysqli_query)){
    $datas[] .= '{
                "transaksi_kode":"'.$r["transaksi_kode"].'",
                "transaksi_tanggal":"'.$r["transaksi_tanggal"].'",                 
                "faktur_pajak_nomor":"'.$r["faktur_pajak_nomor"].'",
                "sisa":"'.$r["sisa"].'",
                "sisa":"'.$r["sisa"].'",
                "kosong":"'.$r["kosong"].'",
                "nol":"'.$r["nol"].'",
                "subtotal":"'.$r["subtotal"].'",
                "keterangan":"'.$r["keterangan"].'",
                "transaksi_tabel":"'.$r["transaksi_tabel"].'",
                "transaksi_nomor":"'.$r["transaksi_nomor"].'",
                "nomormhaccount":"'.$r["nomormhaccount"].'",
                "nomormhaccount":"'.$r["nomormhaccount"].'",
                "tipe":"'.$r["tipe"].'",
                "jenis":"'.$r["jenis"].'",
                "multiplier":"'.$r["multiplier"].'"
              }';
}
echo json_encode($datas);
?>