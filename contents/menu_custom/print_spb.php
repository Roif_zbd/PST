<script type="text/javascript" src="../../assets_custom/js/qrcode/qrcode.js"></script>
<script type="text/javascript" src="../../assets_custom/js/jquery.min.js"></script>
<style>
	*{
		font-family: arial;
	}

	table {
		border-collapse: collapse;
	}

	.table-detail {
		border-top: 1px solid black;
		border-bottom: 1px solid black;
	}

	.table-detail td{
		border-left: 1px solid black;
		border-right: 1px solid black;
	}

	.myheader {
		vertical-align: top;
		padding: 5px 0 0 15px;
	}

	.text-small {
		font-size: 10px;
	}

	.text-medium {
		font-size: 12px;
	}

	.garis {
		border-top: 1px solid black;
	}

</style>

<?php
include "../../config/config.php";
include "../../config/database.php";
include "../../".$config["webspira"]."config/connection.php";

$conn_header = new mysqli($mysql["server"], $mysql["username"], $mysql["password"], $mysql["database"]);
if ($conn_header->connect_error) { die("Connection failed: " . $conn_header->connect_error); }

$sql = "
	SELECT c.kode AS kode_barang, 
	IF(n.nama_alias IS NOT NULL AND n.nama_alias <> '', n.nama_alias, c.nama) AS nama_barang, 
	e.nama AS merk, 
	SUM(a.jumlah-IFNULL((SELECT jumlah FROM tdreturjual WHERE nomortdspb = a.nomor),0)) AS jumlah, 
	IF(n.satuan_alias IS NOT NULL AND n.satuan_alias <> '', n.satuan_alias, IF(n.satuan = '', d.nama, n.satuan)) AS satuan,
	b.kode AS nomorspb,
	i.kode AS nomorso,
	i.kode_po AS nomorpo,
	b.tanggal,
	f.kode AS kode_customer,
	f.nama AS customer,
	f.pic AS penerima,
	f.alamat1 AS alamat_customer,
	i.alamat_kirim AS alamat_kirim,
	IF(b.kode LIKE '%SPK%', i.lokasi_konsinyasi, i.penerima) AS penerima_so,
	i.alamat_kirim AS alamat_kirim_so,
	l.kode AS kode_gdg, b.jumlah_koli 
	FROM tdspb a 
	JOIN thspb b ON a.nomorthspb = b.nomor 
	JOIN mhbarang c ON a.nomormhbarang = c.nomor 
	JOIN mhsatuan d ON a.nomormhsatuan = d.nomor 
	LEFT JOIN mhmerkbarang e ON c.nomormhmerkbarang = e.nomor
	JOIN tdsuratjalan g ON a.nomortdsuratjalan = g.nomor
	JOIN thsuratjalan h ON g.nomorthsuratjalan = h.nomor
	JOIN thso i ON h.nomorthso = i.nomor
	JOIN mhrelasi f ON i.nomormhrelasicustomer = f.nomor
	JOIN tdpickinglist_wave j ON g.nomortdpickinglist_wave = j.nomor
	JOIN mhlokasi k ON j.lokasi_asal = k.nama
	JOIN mhgudang l ON k.nomormhgudang = l.nomor
	JOIN tdpickinglist m ON j.nomortdpickinglist = m.nomor
	JOIN tdso n ON m.nomortdso = n.nomor
	WHERE a.status_aktif = 1 AND a.jumlah > 0 AND a.status_retur = 0 AND a.nomorthspb=". $_GET['no']." GROUP BY n.nomor_urut";
//echo $sql;

$result = $conn_header->query($sql);
$row = $result->fetch_all(MYSQLI_ASSOC);

date_default_timezone_set('Asia/Jakarta');

$index_nomor = 0;
$index_halaman = 1;
$batas_data = 9;
$jumlah_data = count($row);
$jumlah_page = ceil($jumlah_data/$batas_data);
$format_header = "";

$tanggal_spb = date_create($row[0]['tanggal']);

for ($x = 0; $x < $jumlah_page; $x++){
	$title = "SURAT PENGANTAR BARANG";
	if(strpos($row[0]['nomorspb'], "/SPK/") !== false){
		$title = "SURAT PENYERAHAN KONSINYASI";
	}
	$kode_spb = $row[0]['nomorspb'];
	$keterangan = " ";
	$jumlah_koli = $row[0]['jumlah_koli'];
	// $format_header = "<font size='1'><i><b>hal : ".($x+1)."</b></i></font>";
	$format_header = $format_header."
		<table class='table' width='100%' style='height:70px;'>
			<tr>
				<td width='50%' style='vertical-align: top; padding: 0;'>
					<table>
						<tr>
							<td width='20%' class='myheader'>
								<img src='../../assets_custom/img/logo_spl.jpeg' width='100%'></img>
							</td>
						   	<td width='80%' class='myheader'>
						   		<font size='4'><strong>CV. SARANA PRIMA LESTARI</strong></font><br>
						   		<font size='2'>JL. PRAMUKA NO.63 RT.11, BANJARMASIN</font><br>
						   		<font size='2'>TELP : 0511 - 3269593 | FAX: 0511 - 3272142</font><br>
						   		<font size='2'>EMAIL : SPL@INDO.NET.ID</font><br>
						   	</td>
						</tr>
						<tr>
							<td colspan='2' class='myheader text-small'>
								Kepada Yth : ".$row[0]['kode_customer']."<br>
								".$row[0]['customer']."<br><br>
								".$row[0]['alamat_customer']."<br>
							</td>
						</tr>
					</table>
				</td>
			   	<td width='50%' class='myheader'>
					<table class='text-medium'>
						<tr>
							<td><font size='4'><strong>".$title."</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>
						</tr>
					</table>
			   		<table class='text-medium'>
			   			<tr>
			   				<td>Nomor</td>
			   				<td>:</td>
			   				<td>".$row[0]['nomorspb']."</td>
							<td rowspan='4'><div class='qrcode'></div></td>
			   			</tr>
			   			<tr>
			   				<td>Tanggal</td>
			   				<td>:</td>
			   				<td>".date_format($tanggal_spb, "d-m-Y")."</td>
			   			</tr>
			   			<tr>
			   				<td>No. SO</td>
			   				<td>:</td>
			   				<td>".$row[0]['nomorso']."</td>
			   			</tr>
			   			<tr>
			   				<td>No. PO</td>
			   				<td>:</td>
			   				<td>".$row[0]['nomorpo']."</td>
			   			</tr>
			   			<tr>
			   				<td>Penerima</td>
			   				<td>:</td>
			   				<td colspan='2'>".$row[0]['penerima_so']."<br></td>
			   			</tr>
			   			<tr>
			   				<td>Alamat Kirim</td>
			   				<td>:</td>
			   				<td colspan='2'>".substr($row[0]['alamat_kirim_so'],0,85)."</td>
			   			</tr>
			   		</table>
			   	</td>
			</tr>
		</table>";
		// <tr>
		// 	<td colspan='3'>
		// 		".substr($row[0]['alamat_kirim'],0,35)."<br>
		// 	</td>
		// </tr>
	$format_header = $format_header ." <div style='height:15px; margin-left: 15px;' class='text-small'>Gudang ".$row[0]['kode_gdg']."</div>";
	$format_header = $format_header."<table class='table-detail' width='90%' style='margin-bottom: ". ($x < $jumlah_page-1 ? "120px" : "0px"). "; margin-left: 35px;'>";
	$format_header .= "
		<tr class='text-small'>
			<td align='center' style='border-bottom: 1px solid black;'><strong>No.</strong></td>
			<td width='50%' style='border-bottom: 1px solid black;'><strong>NAMA BARANG</strong></td>
			<td style='border-bottom: 1px solid black;'><strong>MERK</strong></td>
			<td style='border-bottom: 1px solid black;'><strong>KODE BARANG</strong></td>
			<td style='border-bottom: 1px solid black;'><strong>SAT</strong></td>
			<td style='border-bottom: 1px solid black;'><strong>QTY</strong></td>
		</tr>
	";
	for ($y = 0; $y < $batas_data; $y++){
		if ($index_nomor < $jumlah_data){
			$array = (array) $row[$index_nomor];
			$format_header = $format_header."
				<tr>
				<td align='center'><font size='2'>".($index_nomor+1)."</font></td>
				<td><font size='2'>".$array['nama_barang']."</font></td>
				<td><font size='2'>".$array['merk']."</font></td>
				<td><font size='2'>".$array['kode_barang']."</font></td>
				<td><font size='2'>".$array['satuan']."</font></td>
				<td><font size='2'>".($array['jumlah'] + 0)."</font></td>
				</tr>";
			$keterangan = ($index_nomor+1).". ".strtoupper($array['alasan'])." ";
			$index_nomor++;
		}
		else{
			$format_header = $format_header."
				<tr style='height:20px;'>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				</tr>";
		}
	}

	// if ($x < $jumlah_page-1) {
	// 	$format_header .= "
	// 		<tr>
	// 			<td class='text-small' colspan='6' style='vertical-align: bottom; padding-top: 120px; border: none;'>". date("d-M-Y H:i:s"). "</td>
	// 		</tr>
	// 	";
	// }
	$format_header = $format_header."</table>";
	if ($x < $jumlah_page-1) {
		$hal = $x + 1;
		$format_header .= "<div class='text-small' style='margin-bottom: 30px; margin-left: 20px;'>".date("d-m-Y H:i:s")."</div>";
		$format_header .= "<div class='text-small' style='margin-bottom: 30px; margin-left: 20px;'>$hal / $jumlah_page</div>";
	}
}

$format_header .= "
	<table class='table text-medium' width='100%'>
		<tr>
			<td colspan=4 width='45%'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Koli : ".$jumlah_koli."</td>
			<td colspan=5 width='55%'>
				<b>*Barang diterima dengan baik dan sudah sesuai dengan yang tertera di Surat Pengantar Barang ini.</b>
			</td>
		</tr>
		<tr>
			<td width='10%'></td>
			<td width='15%' align='center'>Kepala Gudang</td>
			<td width='5%'></td>
			<td width='15%' align='center'>Diperiksa</td>
			<td width='5%'></td>
			<td width='15%' align='center'>Pembawa</td>
			<td width='5%'></td>
			<td width='15%' align='center'>Penerima</td>
			<td width='15%'></td>
		</tr>
		<tr>
			<td width='10%' height='30px'></td>
			<td width='15%' align='center'></td>
			<td width='5%'></td>
			<td width='15%' align='center'></td>
			<td width='5%'></td>
			<td width='15%' align='center'></td>
			<td width='5%'></td>
			<td width='15%' align='center'></td>
			<td width='15%'></td>
		</tr>
		<tr>
			<td width='10%'></td>
			<td width='15%'>Nama<div class='garis'>Tanggal</td>
			<td width='5%'></td>
			<td width='15%'>Nama<div class='garis'>Tanggal</td>
			<td width='5%'></td>
			<td width='15%'>Nama<div class='garis'>Tanggal</td>
			<td width='5%'></td>
			<td width='15%'>Nama<div class='garis'>Tanggal</td>
			<td width='15%'></td>
		</tr>
	<table>
	<div class='text-small' style='padding-top: 5px; margin-left: 20px;'>".date("d-m-Y H:i:s")."</div>
	<div class='text-small' style='padding-top: 5px; margin-left: 20px;'>$jumlah_page / $jumlah_page</div>
";

echo $format_header;

$sql_print_count = "UPDATE thspb SET print_count = (print_count+1) WHERE nomor=". $_GET['no'];
$conn_header->query($sql_print_count);
?>

<script type="text/javascript">
// INT UNTUK GENERATE BARCODE
$(document).ready(function (){
	jQuery('.qrcode').each(function(index, currentElement) {
        new QRCode(currentElement, {
            text: "<?php echo $kode_spb; ?>",
            width: 50,
            height: 50,
            correctLevel : QRCode.CorrectLevel.H
        });
    });
});
</script>