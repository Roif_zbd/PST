<?php
$edit["debug"]                  = 1;
$edit["unique"]                 = array("kode","nama");
$edit["string_code"] = "kode_".$_SESSION["menu_".$_SESSION["g.menu"]]["string"];

$i = 0;
if(!empty($_GET["a"]) && $_GET["a"] == "view")
    $edit["field"][$i]["box_tabs"]                      = array("data|Data","info|Info");
$edit["field"][$i]["box_tab"]                           = "data";
$edit["field"][$i]["label"]                             = "Kode";
$edit["field"][$i]["label_class"]                       = "req";
$edit["field"][$i]["input"]                             = "kode";
$edit["field"][$i]["input_class"]                       = "required";
$edit["field"][$i]["input_attr"]["maxlength"]           = "200";
$edit["field"][$i]["form_group_class"] 			        = "hiding";
$edit["field"][$i]["input_attr"]["readonly"]    = "readonly";
if($edit["mode"] == "add")
    $edit["field"][$i]["input_value"] = formatting_code($con, $edit["string_code"]);
$i++;
$edit["field"][$i]["label"]                             = "Kelurahan";
$edit["field"][$i]["label_class"]                       = "req";
$edit["field"][$i]["input"]                             = "nama_kelurahan";
$edit["field"][$i]["input_class"]                       = "required";
$edit["field"][$i]["input_attr"]["maxlength"]           = "200";
$i++;
$edit["field"][$i]["label"]                             = "Kecamatan";
$edit["field"][$i]["label_class"]                       = "req";
$edit["field"][$i]["input"]                             = "nomormhkecamatan";
$edit["field"][$i]["input_class"]                       = "required";
$edit["field"][$i]["input_attr"]["maxlength"]           = "200";
$edit["field"][$i]["input_element"]                          = "browse";
$edit["field"][$i]["browse_setting"]                         = "master_kecamatan";
$edit["field"][$i]["browse_set"]["param_output"]             = array(
    "info2|nama_kota",
    "info3|nama_provinsi"
);
$i++;
$edit["field"][$i]["label"]                             = "Kota";
$edit["field"][$i]["label_class"]                       = "req";
$edit["field"][$i]["input"]                             = "nama_kota";
$edit["field"][$i]["input_class"]                       = "required";
$edit["field"][$i]["input_attr"]["maxlength"]           = "200";
$edit["field"][$i]["input_attr"]["readonly"]            = "readonly";
$edit["field"][$i]["input_save"] = "skip";
$i++;
$edit["field"][$i]["label"]                             = "Provinsi";
$edit["field"][$i]["label_class"]                       = "req";
$edit["field"][$i]["input"]                             = "nama_provinsi";
$edit["field"][$i]["input_class"]                       = "required";
$edit["field"][$i]["input_attr"]["maxlength"]           = "200";
$edit["field"][$i]["input_attr"]["readonly"]    = "readonly";
$edit["field"][$i]["input_save"] = "skip";
$i++;
// $edit["field"][$i]["form_group"]                     = 0;
// $edit["field"][$i]["label"]                             = "Keterangan";
// $edit["field"][$i]["input"]                             = "keterangan";
// $edit["field"][$i]["input_element"]                     = "textarea";
// $edit["field"][$i]["input_attr"]["rows"]                = "3";
// $edit["field"][$i]["input_attr"]["cols"]                = "44";
// $i++;
$edit["field"][$i]["anti_mode"]                         = "add";
$edit["field"][$i]["label"]                             = "Status";
$edit["field"][$i]["input"]                             = "status_aktif";
$edit["field"][$i]["input_element"]                     = "select1";
$edit["field"][$i]["input_option"]                      = generate_status_option($edit["mode"]);
$i++;

if($edit["mode"] != "add")
{
    $edit["query"] = "
    SELECT a.*,
    CONCAT(b.nama_kecamatan, '|', b.nomor) AS 'browse|nomormhkecamatan',
    c.nama_kota as nama_kota,
    d.nama_provinsi as nama_provinsi
    FROM ".$edit["table"]." as a
    join mhkecamatan as b 
    on a.nomormhkecamatan = b.nomor
    join mhkota as c
    on b.nomormhkota = c.nomor
    join mhprovinsi as d 
    on c.nomormhprovinsi = d.nomor
    WHERE a.nomor = ".$_GET["no"];
    
	if($edit["debug"] > 0)
        echo $edit["query"];
    $r = mysqli_fetch_array(mysqli_query($con, $edit["query"]));

    $edit = fill_value($edit,$r);

}

$edit = generate_info($edit,$r,"insert|update|delete");
$edit_navbutton = generate_navbutton($edit_navbutton);


?>

<script language="javascript" type="text/javascript">
$(document).ready(function() {

    <?php if($edit["mode"] != "add") { ?>
    var merk_edit = "<?=$r["merk"]?>";
    $("#browse_browse_merk_alat_kalibrasi_search").val(merk_edit);
    $("#browse_browse_merk_alat_kalibrasi_hidden").val(merk_edit);
    <?php } ?>

    $("#browse_browse_merk_alat_kalibrasi_search").blur(function(e) {
        var merk = $("#browse_browse_merk_alat_kalibrasi_search").val();
        // console.log(merk);
        $("#browse_browse_merk_alat_kalibrasi_hidden").val(merk);
    });
});

function checkHeader() {
    var fields = [
        "kode|Kode"
    ];
    var check_failed = check_value_elements(fields);
    if (check_failed != '')
        return check_failed;
    else
        return true;
}

<?=generate_function_checkgrid($grid_str)?>
<?=generate_function_checkunique($grid_str)?>
<?=generate_function_realgrid($grid_str)?>
</script>