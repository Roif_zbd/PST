<?php if($_POST["kategori"] == "PEGAWAI"){ ?>

		<?php
		/*
		if(isset($_FILES["file_upload"]["name"]) && !empty($_FILES["file_upload"]["name"]) && $_FILES["file_upload"]["size"] > 0)
		{
			unlink($_SESSION["setting"]["dir_upload"]."/".$r["directory"]);
			$random = rand(1,9999);
			$file = "quotation/".$random."-".$_FILES["file_upload"]["name"];
			$location = getcwd()."/"."uploads/quotation/".$random."-".$_FILES["file_upload"]["name"];
			move_uploaded_file($_FILES["file_upload"]["tmp_name"],$_SESSION["setting"]["dir_upload"]."/".$file);
			$_POST["nama"] 		= $random."-".$_FILES["file_upload"]["name"];
			$_POST["directory"] = $file;
			// exit;
		}
		*/
		// ======= Standard Upload: begin =============================================================================================
		$counter = $_POST["counter"];

		$kategori = array("KTP", "NPWP", "KK", "SURAT NIKAH", "AKTA LAHIR", "IJAZAH SD", "IJAZAH SMP", "IJAZAH SMA", "IJAZAH PERGURUAN TINGGI", "KURSUS", "PENGHARGAAN", "SKCK", "SURAT KETERANGAN SEHAT","SURAT KETERANGAN BEBAS NARKOBA");

		transactions("start");

		for($i = 0; $i < $counter; $i++){
			$up_file = $_FILES["file_upload_".$i];
			$up_dir = $_SESSION["setting"]["dir_upload"];
			$up_folder = $kategori[$i];
			$up_field = "directory";
			$unlink_old = 1;
			$random_name = 9999;

			if(isset($up_file["name"]) && !empty($up_file["name"]) && $up_file["size"] > 0)
			{
				if($unlink_old == 1)
					unlink($up_dir."/".$r[$up_field]);
				
				$dest_folder = $up_dir;
				if(!empty($up_folder))
					$dest_folder .= "/".$up_folder;
				
				$dest_name = $up_file["name"];
				if(!empty($random_name))
					$dest_name = rand(1,$random_name)."-".$up_file["name"];
				
				$uploaded = upload_file($up_file["tmp_name"],$dest_folder,$dest_name);
				if($uploaded == "succeed"){
					$_POST[$up_field] = $up_folder."/".$dest_name;

					$_POST["nama"] = $dest_name;
					$_POST["kategori"] = $kategori[$i];
					$_POST["directory"] = $kategori[$i] . "/". $dest_name;


					$insert = mysqli_query($con, "UPDATE mharchievedfiles SET status_aktif = 0 WHERE kodefile = '". $_POST["kodefile"] ."' AND namatable = '". $_POST["namatable"] ."' AND kategori = '". $kategori[$i] ."'");
					if(!$insert)
					{
						transactions("rollback");
						set_alert(get_message(703,"Setting Application Data"),"danger","",$url);
					}
					
					$insert = mysqli_query($con, "
					INSERT INTO mharchievedfiles ( kode, nomorfile, kodefile, namatable, kategori, nama, directory ) 
					VALUES ((SELECT CONCAT('AR',(SELECT kode FROM mhcabang WHERE nomor = ".$_SESSION["login"]["cabang"]."),DATE_FORMAT(NOW(),'%y%m'),LPAD((SUBSTRING(IFNULL(MAX(kode),1),10)+1),4,'0')) AS kode 
				   FROM mharchievedfiles a),". $_POST["nomorfile"] .",'". $_POST["kodefile"] ."','". $_POST["namatable"] ."','". $kategori[$i] ."','". $dest_name ."','".  $kategori[$i] . "/". $dest_name ."')
					");
					if(!$insert)
					{
						transactions("rollback");
						set_alert(get_message(703,"Setting Application Data"),"danger","",$url);
					}
				}
				else
				{
					$additional_message = "";
					if($uploaded == "not_writeable")
						$additional_message = ", destination folder (".$dest_folder.") is not writeable.";
					set_alert(get_message(703,"Upload file").$additional_message,"danger");
				}
			}
		}



		//======== Jumlah Anak ========================================================================================================

		$counter = $_POST["jumlah_anak"];

		if($counter > 0){
			$insert = mysqli_query($con, "UPDATE mharchievedfiles SET status_aktif = 0 WHERE kodefile = '". $_POST["kodefile"] ."' AND namatable = '". $_POST["namatable"] ."' AND kategori = 'AKTA LAHIR ANAK'");
			if(!$insert)
			{
				transactions("rollback");
				set_alert(get_message(703,"Setting Application Data"),"danger","",$url);
			}
		}

		for($i = 0; $i < $counter; $i++){
			$up_file = $_FILES["file_upload_aktaanak_".$i];
			$up_dir = $_SESSION["setting"]["dir_upload"];
			$up_folder = "AKTA LAHIR ANAK";
			$up_field = "directory";
			$unlink_old = 1;
			$random_name = 9999;


			if(isset($up_file["name"]) && !empty($up_file["name"]) && $up_file["size"] > 0)
			{
				if($unlink_old == 1)
					unlink($up_dir."/".$r[$up_field]);
				
				$dest_folder = $up_dir;
				if(!empty($up_folder))
					$dest_folder .= "/".$up_folder;
				
				$dest_name = $up_file["name"];
				if(!empty($random_name))
					$dest_name = rand(1,$random_name)."-".$up_file["name"];
				
				$uploaded = upload_file($up_file["tmp_name"],$dest_folder,$dest_name);
				if($uploaded == "succeed"){
					$_POST[$up_field] = $up_folder."/".$dest_name;

					$_POST["nama"] = $dest_name;
					$_POST["kategori"] = "AKTA LAHIR ANAK";
					$_POST["directory"] = $_POST["kategori"] . "/". $dest_name;

					$insert = mysqli_query($con, "
					INSERT INTO mharchievedfiles ( kode, nomorfile, kodefile, namatable, kategori, nama, directory ) 
					VALUES ((SELECT CONCAT('AR',(SELECT kode FROM mhcabang WHERE nomor = ".$_SESSION["login"]["cabang"]."),DATE_FORMAT(NOW(),'%y%m'),LPAD((SUBSTRING(IFNULL(MAX(kode),1),10)+1),4,'0')) AS kode 
				   FROM mharchievedfiles a),". $_POST["nomorfile"] .",'". $_POST["kodefile"] ."','". $_POST["namatable"] ."','". $_POST["kategori"] ."','". $dest_name ."','".  $_POST["kategori"] . "/". $dest_name ."')
					");
					if(!$insert)
					{
						transactions("rollback");
						set_alert(get_message(703,"Setting Application Data"),"danger","",$url);
					}
				}
				else
				{
					$additional_message = "";
					if($uploaded == "not_writeable")
						$additional_message = ", destination folder (".$dest_folder.") is not writeable.";
					set_alert(get_message(703,"Upload file").$additional_message,"danger");
				}
			}
		}


		transactions("commit");
		// ======= Standard Upload: end ===============================================================================================
		echo "<script>window.close();</script>";
		exit();
		?>

<?php }else{ ?>

		<?php
		/*
		if(isset($_FILES["file_upload"]["name"]) && !empty($_FILES["file_upload"]["name"]) && $_FILES["file_upload"]["size"] > 0)
		{
			unlink($_SESSION["setting"]["dir_upload"]."/".$r["directory"]);
			$random = rand(1,9999);
			$file = "quotation/".$random."-".$_FILES["file_upload"]["name"];
			$location = getcwd()."/"."uploads/quotation/".$random."-".$_FILES["file_upload"]["name"];
			move_uploaded_file($_FILES["file_upload"]["tmp_name"],$_SESSION["setting"]["dir_upload"]."/".$file);
			$_POST["nama"] 		= $random."-".$_FILES["file_upload"]["name"];
			$_POST["directory"] = $file;
			// exit;
		}
		*/
		// ======= Standard Upload: begin =============================================================================================
		$counter = $_POST["counter"];


		transactions("start");

		for($i = 0; $i < $counter; $i++){
			$up_file = $_FILES["file_upload_".$i];
			$up_dir = $_SESSION["setting"]["dir_upload"];
			$up_folder = $_POST["kategori"];
			$up_field = "directory";
			$unlink_old = 1;
			$random_name = 9999;

			if(isset($up_file["name"]) && !empty($up_file["name"]) && $up_file["size"] > 0)
			{
				if($unlink_old == 1)
					unlink($up_dir."/".$r[$up_field]);
				
				$dest_folder = $up_dir;
				if(!empty($up_folder))
					$dest_folder .= "/".$up_folder;
				
				$dest_name = $up_file["name"];
				if(!empty($random_name))
					$dest_name = rand(1,$random_name)."-".str_replace(" ","_",$up_file["name"]);
				
				$uploaded = upload_file($up_file["tmp_name"],$dest_folder,$dest_name);
				if($uploaded == "succeed"){
					$_POST[$up_field] = $up_folder."/".$dest_name;

					if($i < $counter - 1){
						$insert = mysqli_query($con, "
						INSERT INTO mharchievedfiles ( kode, nomorfile, kodefile, namatable, kategori, nama, directory, keterangan ) 
						VALUES ((SELECT CONCAT('AR',(SELECT kode FROM mhcabang WHERE nomor = ".$_SESSION["cabang"]["nomor"]."),DATE_FORMAT(NOW(),'%y%m'),LPAD((SUBSTRING(IFNULL(MAX(kode),1),10)+1),4,'0')) AS kode 
					   FROM mharchievedfiles a),". $_POST["nomorfile"] .",'". $_POST["kodefile"] ."','". $_POST["namatable"] ."','". $_POST["kategori"] ."','". $dest_name ."','". $_POST["directory"] ."','". $_POST["keterangan"] ."')
						");
						if(!$insert)
						{
							transactions("rollback");
							set_alert(get_message(703,"Setting Application Data"),"danger","",$url);
						}
					}
				}
				else
				{
					$additional_message = "";
					if($uploaded == "not_writeable")
						$additional_message = ", destination folder (".$dest_folder.") is not writeable.";
					set_alert(get_message(703,"Upload file").$additional_message,"danger");
				}
			}
		}

		if($_POST["kategori"] == "PEGAWAI_ABSEN_IJIN" || $_POST["kategori"] == "REIMBURSEMENT" || $_POST["kategori"] == "LOAN"){
			mysqli_query($con, "UPDATE ".$_POST["namatable"]." SET is_upload = 1 WHERE nomor = ".$_POST["nomorfile"]);
			mysqli_query($con, "UPDATE ".$_POST["namatable"]." SET status_aktif = 1 WHERE nomor = ".$_POST["nomorfile"]);
		}


		transactions("commit");
		// ======= Standard Upload: end ===============================================================================================

		$_POST["nama"] = $dest_name;
		?>


<?php } ?>