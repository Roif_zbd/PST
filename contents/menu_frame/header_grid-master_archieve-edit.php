<?php if($_GET["tipe"] == "PEGAWAI"){ ?>
	<?php

	if(isset($_GET["no"])){
		$noIDHeader = $_GET["no"];
	}else{
		$tableHeader = $_GET["table"];
		$idHeader 	 = $_GET["id"];
		$tipeHeader  = $_GET["tipe"];

		$queryHeader = "
						SELECT a.* 
						FROM ".$tableHeader." a
						WHERE a.nomor = ".$idHeader;
		$resultHeader = mysqli_query($con, $queryHeader);
		$rowHeader = mysqli_fetch_array($resultHeader);
	}

		$queryHeaderAktalahir = "SELECT * 
								FROM mharchievedfiles 
								WHERE status_aktif = 1 AND 
										namatable = 'mhpegawai' AND 
										kategori = 'AKTA LAHIR' AND 
										nomorfile = $idHeader";
		$resultHeaderAktalahir = mysqli_query($con, $queryHeaderAktalahir);
		$rowHeaderAktalahir = mysqli_num_rows($resultHeaderAktalahir);

		$queryHeaderKTP = "SELECT * 
								FROM mharchievedfiles 
								WHERE status_aktif = 1 AND 
										namatable = 'mhpegawai' AND 
										kategori = 'KTP' AND 
										nomorfile = $idHeader";
		$resultHeaderKTP = mysqli_query($con, $queryHeaderKTP);
		$rowHeaderKTP = mysqli_num_rows($resultHeaderKTP);

		$queryHeaderKK = "SELECT * 
								FROM mharchievedfiles 
								WHERE status_aktif = 1 AND 
										namatable = 'mhpegawai' AND 
										kategori = 'KK' AND 
										nomorfile = $idHeader";
		$resultHeaderKK = mysqli_query($con, $queryHeaderKK);
		$rowHeaderKK = mysqli_num_rows($resultHeaderKK);

		$queryHeaderSuratNikah = "SELECT * 
								FROM mharchievedfiles 
								WHERE status_aktif = 1 AND 
										namatable = 'mhpegawai' AND 
										kategori = 'SURAT NIKAH' AND 
										nomorfile = $idHeader";
		$resultHeaderSuratNikah = mysqli_query($con, $queryHeaderSuratNikah);
		$rowHeaderSuratNikah = mysqli_num_rows($resultHeaderSuratNikah);

		$queryHeaderAktaAnak = "SELECT * 
								FROM mharchievedfiles 
								WHERE status_aktif = 1 AND 
										namatable = 'mhpegawai' AND 
										kategori = 'AKTA LAHIR ANAK' AND 
										nomorfile = $idHeader";
		$resultHeaderAktaAnak = mysqli_query($con, $queryHeaderAktaAnak);
		$rowHeaderAktaAnak = mysqli_num_rows($resultHeaderAktaAnak);

	$edit["url_success"] = $_SESSION["g.url"]."&f=header_grid&";

	$edit["unique"] = array("nama");
	$edit["string_code"] = "kode_".$_SESSION["menu_".$_SESSION["g.menu"]]["string"];
	$edit["manual_save"] = "contents/menu_frame/".$_SESSION["g.frame"]."-".$_SESSION["g.menu"]."-save.php";
	$edit["debug"] = 1;

	$i = 0;
	if(!empty($_GET["a"]) && $_GET["a"] == "view")
		$edit["field"][$i]["box_tabs"] = array("data|Data","info|Info");
	$edit["field"][$i]["box_tab"] = "data";
	$edit["field"][$i]["label"] = "Archieved ID";
	$edit["field"][$i]["label_col"] = "col-sm-3";
	$edit["field"][$i]["input"] = "kode";
	$edit["field"][$i]["input_attr"]["maxlength"] = "100";
	$edit["field"][$i]["input_attr"]["readonly"] = "readonly";
	if($edit["mode"] == "add")
		$edit["field"][$i]["input_value"] = formatting_code($edit["string_code"]);
	$i++;
	$edit["field"][$i]["label"] = "Category";
	$edit["field"][$i]["label_col"] = "col-sm-3";
	$edit["field"][$i]["input"] = "kategori";
	$edit["field"][$i]["input_attr"]["maxlength"] = "200";
	$edit["field"][$i]["input_attr"]["readonly"] = "readonly";
	if(mysqli_num_rows($resultHeader) > 0)
		$edit["field"][$i]["input_attr"]["value"] = $tipeHeader;
	$i++;
	$edit["field"][$i]["form_group_class"] = "hiding";
	$edit["field"][$i]["label"] = "File Name";
	$edit["field"][$i]["label_col"] = "col-sm-3";
	$edit["field"][$i]["input"] = "nama";
	$edit["field"][$i]["input_attr"]["maxlength"] = "200";
	$edit["field"][$i]["input_attr"]["readonly"] = "readonly";
	$i++;
	$edit["field"][$i]["label"] = "Nama";
	$edit["field"][$i]["label_col"] = "col-sm-3";
	$edit["field"][$i]["input"] = "kodefile";
	$edit["field"][$i]["input_attr"]["maxlength"] = "200";
	$edit["field"][$i]["input_attr"]["readonly"] = "readonly";
	if(mysqli_num_rows($resultHeader) > 0)
		$edit["field"][$i]["input_attr"]["value"] = $rowHeader["nama"];
	$i++;
	$edit["field"][$i]["form_group_class"] = "hiding";
	$edit["field"][$i]["label"] = "File Nomor";
	$edit["field"][$i]["label_col"] = "col-sm-3";
	$edit["field"][$i]["input"] = "nomorfile";
	$edit["field"][$i]["input_attr"]["maxlength"] = "200";
	$edit["field"][$i]["input_attr"]["readonly"] = "readonly";
	if(mysqli_num_rows($resultHeader) > 0)
		$edit["field"][$i]["input_attr"]["value"] = $rowHeader["nomor"];
	$i++;
	$edit["field"][$i]["form_group_class"] = "hiding";
	$edit["field"][$i]["label"] = "Table Name";
	$edit["field"][$i]["label_col"] = "col-sm-3";
	$edit["field"][$i]["input"] = "namatable";
	$edit["field"][$i]["input_attr"]["maxlength"] = "200";
	$edit["field"][$i]["input_attr"]["readonly"] = "readonly";
	if(mysqli_num_rows($resultHeader) > 0)
		$edit["field"][$i]["input_attr"]["value"] = $tableHeader;
	$i++;
	$edit["field"][$i]["form_group_class"] = "hiding";
	$edit["field"][$i]["label"] = "Directory";
	$edit["field"][$i]["label_col"] = "col-sm-3";
	$edit["field"][$i]["input"] = "directory";
	$edit["field"][$i]["input_attr"]["maxlength"] = "200";
	$edit["field"][$i]["input_attr"]["readonly"] = "readonly";
	$edit["field"][$i]["input_attr"]["value"] = "quotation";
	$i++;
	$edit["field"][$i]["form_group_class"] = "hiding";
	$edit["field"][$i]["label"] = "Counter";
	$edit["field"][$i]["label_col"] = "col-sm-3";
	$edit["field"][$i]["input"] = "counter";
	$edit["field"][$i]["input_save"] = "skip";
	$edit["field"][$i]["input_attr"]["readonly"] = "readonly";
	$edit["field"][$i]["input_attr"]["value"] = "14";
	$i++;
	$edit["field"][$i]["form_group_class"] = "hiding";
	$edit["field"][$i]["label"] = "Jumlah Anak";
	$edit["field"][$i]["label_col"] = "col-sm-3";
	$edit["field"][$i]["input"] = "jumlah_anak";
	$edit["field"][$i]["input_attr"]["maxlength"] = "200";
	$edit["field"][$i]["input_attr"]["readonly"] = "readonly";
	if(mysqli_num_rows($resultHeader) > 0)
		$edit["field"][$i]["input_attr"]["value"] = $_GET["ja"];
	$i++;
	// $edit["field"][$i]["label"] = " ";

	// $locationprint = "'pages/print_invoice.php?m=".$_SESSION["menu_".$_SESSION["g.menu"]]["string"]."&f=header_grid&&sm=edit&a=view&tipe=bl&no=".$_GET["no"]."','_newtab'";
	// $edit["field"][$i]["input_element"] = '<a class="btn btn-default btn-xs btn_print" style="width:350px;" onclick="tambahUploadFile()">&nbsp;&nbsp;Add Upload File&nbsp;&nbsp;</a>';

	// $edit["field"][$i]["input_save"] = "skip";
	// $i++;
	$edit["field"][$i]["anti_mode"] = "view";
	$edit["field"][$i]["label"] = "KTP";
	$edit["field"][$i]["label_col"] = "col-sm-3";
	$edit["field"][$i]["input"] = "file_upload_0";
	$edit["field"][$i]["input_attr"]["type"] = "file";
	$edit["field"][$i]["input_attr"]["accept"] = "*";
	$edit["field"][$i]["input_save"] = "skip";
	if($rowHeaderKTP == "0"){
		$edit["field"][$i]["label_class"] = "req";
		$edit["field"][$i]["input_attr"]["required"] = "true";	
	}
	$i++;
	$edit["field"][$i]["anti_mode"] = "view";
	$edit["field"][$i]["label"] = "NPWP";
	$edit["field"][$i]["label_col"] = "col-sm-3";
	$edit["field"][$i]["input"] = "file_upload_1";
	$edit["field"][$i]["input_attr"]["type"] = "file";
	$edit["field"][$i]["input_attr"]["accept"] = "*";
	$edit["field"][$i]["input_save"] = "skip";
	$i++;
	$edit["field"][$i]["anti_mode"] = "view";
	$edit["field"][$i]["label"] = "KK";
	$edit["field"][$i]["label_col"] = "col-sm-3";
	$edit["field"][$i]["input"] = "file_upload_2";
	$edit["field"][$i]["input_attr"]["type"] = "file";
	$edit["field"][$i]["input_attr"]["accept"] = "*";
	$edit["field"][$i]["input_save"] = "skip";
	if($rowHeaderKK == "0"){
		$edit["field"][$i]["label_class"] = "req";
		$edit["field"][$i]["input_attr"]["required"] = "true";	
	}
	$i++;
	$edit["field"][$i]["anti_mode"] = "view";
	$edit["field"][$i]["label"] = "Surat Nikah";
	$edit["field"][$i]["label_col"] = "col-sm-3";
	$edit["field"][$i]["input"] = "file_upload_3";
	$edit["field"][$i]["input_attr"]["type"] = "file";
	$edit["field"][$i]["input_attr"]["accept"] = "*";
	$edit["field"][$i]["input_save"] = "skip";
	if($_GET["n"] == "2"){
		if($rowHeaderSuratNikah == "0"){
			$edit["field"][$i]["label_class"] = "req";
			$edit["field"][$i]["input_attr"]["required"] = "true";	
		}
	}
	$i++;
	$edit["field"][$i]["anti_mode"] = "view";
	$edit["field"][$i]["label"] = "Akta Lahir";
	$edit["field"][$i]["label_col"] = "col-sm-3";
	$edit["field"][$i]["input"] = "file_upload_4";
	$edit["field"][$i]["input_attr"]["type"] = "file";
	$edit["field"][$i]["input_attr"]["accept"] = "*";
	$edit["field"][$i]["input_save"] = "skip";
	if($rowHeaderAktalahir == "0"){
		$edit["field"][$i]["label_class"] = "req";
		$edit["field"][$i]["input_attr"]["required"] = "true";	
	}
	$i++;
	$edit["field"][$i]["anti_mode"] = "view";
	$edit["field"][$i]["label"] = "Ijazah SD";
	// $edit["field"][$i]["label_class"] = "req";
	$edit["field"][$i]["label_col"] = "col-sm-3";
	$edit["field"][$i]["input"] = "file_upload_5";
	$edit["field"][$i]["input_attr"]["type"] = "file";
	$edit["field"][$i]["input_attr"]["accept"] = "*";
	$edit["field"][$i]["input_save"] = "skip";
	// $edit["field"][$i]["input_attr"]["required"] = "true";
	$i++;
	$edit["field"][$i]["anti_mode"] = "view";
	$edit["field"][$i]["label"] = "Ijazah SMP";
	// $edit["field"][$i]["label_class"] = "req";
	$edit["field"][$i]["label_col"] = "col-sm-3";
	$edit["field"][$i]["input"] = "file_upload_6";
	$edit["field"][$i]["input_attr"]["type"] = "file";
	$edit["field"][$i]["input_attr"]["accept"] = "*";
	$edit["field"][$i]["input_save"] = "skip";
	// $edit["field"][$i]["input_attr"]["required"] = "true";
	$i++;
	$edit["field"][$i]["anti_mode"] = "view";
	$edit["field"][$i]["label"] = "Ijazah SMA";
	// $edit["field"][$i]["label_class"] = "req";
	$edit["field"][$i]["label_col"] = "col-sm-3";
	$edit["field"][$i]["input"] = "file_upload_7";
	$edit["field"][$i]["input_attr"]["type"] = "file";
	$edit["field"][$i]["input_attr"]["accept"] = "*";
	$edit["field"][$i]["input_save"] = "skip";
	// $edit["field"][$i]["input_attr"]["required"] = "true";
	$i++;
	$edit["field"][$i]["anti_mode"] = "view";
	$edit["field"][$i]["label"] = "Ijazah Perguruan Tinggi";
	// $edit["field"][$i]["label_class"] = "req";
	$edit["field"][$i]["label_col"] = "col-sm-3";
	$edit["field"][$i]["input"] = "file_upload_8";
	$edit["field"][$i]["input_attr"]["type"] = "file";
	$edit["field"][$i]["input_attr"]["accept"] = "*";
	$edit["field"][$i]["input_save"] = "skip";
	// $edit["field"][$i]["input_attr"]["required"] = "true";
	$i++;
	$edit["field"][$i]["anti_mode"] = "view";
	$edit["field"][$i]["label"] = "Kursus";
	$edit["field"][$i]["label_col"] = "col-sm-3";
	$edit["field"][$i]["input"] = "file_upload_9";
	$edit["field"][$i]["input_attr"]["type"] = "file";
	$edit["field"][$i]["input_attr"]["accept"] = "*";
	$edit["field"][$i]["input_save"] = "skip";
	$i++;
	$edit["field"][$i]["anti_mode"] = "view";
	$edit["field"][$i]["label"] = "Penghargaan";
	$edit["field"][$i]["label_col"] = "col-sm-3";
	$edit["field"][$i]["input"] = "file_upload_10";
	$edit["field"][$i]["input_attr"]["type"] = "file";
	$edit["field"][$i]["input_attr"]["accept"] = "*";
	$edit["field"][$i]["input_save"] = "skip";
	$i++;
	$edit["field"][$i]["anti_mode"] = "view";
	$edit["field"][$i]["label"] = "SKCK";
	$edit["field"][$i]["label_col"] = "col-sm-3";
	$edit["field"][$i]["input"] = "file_upload_11";
	$edit["field"][$i]["input_attr"]["type"] = "file";
	$edit["field"][$i]["input_attr"]["accept"] = "*";
	$edit["field"][$i]["input_save"] = "skip";
	$i++;
	$edit["field"][$i]["anti_mode"] = "view";
	$edit["field"][$i]["label"] = "Surat Keterangan Sehat";
	$edit["field"][$i]["label_col"] = "col-sm-3";
	$edit["field"][$i]["input"] = "file_upload_12";
	$edit["field"][$i]["input_attr"]["type"] = "file";
	$edit["field"][$i]["input_attr"]["accept"] = "*";
	$edit["field"][$i]["input_save"] = "skip";
	$i++;
	$edit["field"][$i]["anti_mode"] = "view";
	$edit["field"][$i]["label"] = "Surat Keterangan Bebas Narkoba";
	$edit["field"][$i]["label_col"] = "col-sm-3";
	$edit["field"][$i]["input"] = "file_upload_13";
	$edit["field"][$i]["input_attr"]["type"] = "file";
	$edit["field"][$i]["input_attr"]["accept"] = "*";
	$edit["field"][$i]["input_save"] = "skip";
	$i++;
	$edit["field"][$i]["anti_mode"] = "add";
	$edit["field"][$i]["label"] = "Status";
	$edit["field"][$i]["input"] = "status_aktif";
	$edit["field"][$i]["input_element"] = "select";
	$edit["field"][$i]["input_option"] = generate_status_option($edit["mode"]);
	$i++;

	for($x = 0; $x < $_GET["ja"]; $x++){
		$edit["field"][$i]["anti_mode"] = "view";
		$edit["field"][$i]["label"] = "Akta Anak ke - " .($x+1);
		$edit["field"][$i]["label_col"] = "col-sm-3";
		$edit["field"][$i]["input"] = "file_upload_aktaanak_".$x;
		$edit["field"][$i]["input_attr"]["type"] = "file";
		$edit["field"][$i]["input_attr"]["accept"] = "*";
		$edit["field"][$i]["input_save"] = "skip";
		if( ($x + 1 ) > $rowHeaderAktaAnak){
			$edit["field"][$i]["label_class"] = "req";
			$edit["field"][$i]["input_attr"]["required"] = "true";	
		}
		$i++;
	}


	if($edit["mode"] != "add")
	{
		$queryData = "SELECT a.* FROM ".$edit["table"]." a WHERE a.nomor = ".$_GET["no"];
		$r = mysqli_fetch_array(mysqli_query($con, $queryData));	
		$edit = fill_value($edit,$r);
	}
	$edit = generate_info($edit,$r,"insert|update");
	$features = "save|back|edit";
	$edit_navbutton = generate_navbutton($edit_navbutton,$features);
	?>

	<script type="text/javascript">
		$(document).ready(function(){
			/*setTimeout(function(){
			  	load_grid_detail();
			}, 10000);*/
			$("#counter").val("14");
		});

		var counter = 1;
		function tambahUploadFile(){
			var counter = $("#counter").val();
			var uploadfilebaru = '<input type="file" accept="*" class="form-control" id="file_upload_'+ counter +'" name="file_upload_'+ counter +'">';
			$( "div[name='div_I_10']" ).append( uploadfilebaru );
			counter++;
			$("#counter").val( counter );
		}
	</script>
<?php }else{ ?>


		<?php

		if(isset($_GET["no"])){
			$noIDHeader = $_GET["no"];
		}else{
			$tableHeader = $_GET["table"];
			$idHeader 	 = $_GET["id"];
			$tipeHeader  = $_GET["tipe"];

			$queryHeader = "
							SELECT a.* 
							FROM ".$tableHeader." a
							WHERE a.nomor = ".$idHeader;
			$resultHeader = mysqli_query($con, $queryHeader);
			$rowHeader = mysqli_fetch_array($resultHeader);
		}

		$edit["url_success"] = $_SESSION["g.url"]."&f=header_grid&";

		$edit["unique"] = array("nama");
		$edit["string_code"] = "kode_".$_SESSION["menu_".$_SESSION["g.menu"]]["string"];
		$edit["manual_save"] = "contents/menu_frame/".$_SESSION["g.frame"]."-".$_SESSION["g.menu"]."-save.php";
		$edit["debug"] = 1;

		$i = 0;
		if(!empty($_GET["a"]) && $_GET["a"] == "view")
			$edit["field"][$i]["box_tabs"] = array("data|Data","info|Info");
		$edit["field"][$i]["box_tab"] = "data";
		$edit["field"][$i]["label"] = "Archieved ID";
		$edit["field"][$i]["input"] = "kode";
		$edit["field"][$i]["input_attr"]["maxlength"] = "100";
		$edit["field"][$i]["input_attr"]["readonly"] = "readonly";
		if($edit["mode"] == "add")
			$edit["field"][$i]["input_value"] = formatting_code($edit["string_code"]);
		$i++;
		$edit["field"][$i]["label"] = "Category";
		$edit["field"][$i]["input"] = "kategori";
		$edit["field"][$i]["input_attr"]["maxlength"] = "200";
		$edit["field"][$i]["input_attr"]["readonly"] = "readonly";
		if(mysqli_num_rows($resultHeader) > 0)
			$edit["field"][$i]["input_attr"]["value"] = $tipeHeader;
		$i++;
		$edit["field"][$i]["form_group_class"] = "hiding";
		$edit["field"][$i]["label"] = "File Name";
		$edit["field"][$i]["input"] = "nama";
		$edit["field"][$i]["input_attr"]["maxlength"] = "200";
		$edit["field"][$i]["input_attr"]["readonly"] = "readonly";
		$i++;
		$edit["field"][$i]["label"] = "File ID";
		$edit["field"][$i]["input"] = "kodefile";
		$edit["field"][$i]["input_attr"]["maxlength"] = "200";
		$edit["field"][$i]["input_attr"]["readonly"] = "readonly";
		if(mysqli_num_rows($resultHeader) > 0)
			$edit["field"][$i]["input_attr"]["value"] = $rowHeader["kode"];
		$i++;
		$edit["field"][$i]["form_group_class"] = "hiding";
		$edit["field"][$i]["label"] = "File Nomor";
		$edit["field"][$i]["input"] = "nomorfile";
		$edit["field"][$i]["input_attr"]["maxlength"] = "200";
		$edit["field"][$i]["input_attr"]["readonly"] = "readonly";
		if(mysqli_num_rows($resultHeader) > 0)
			$edit["field"][$i]["input_attr"]["value"] = $rowHeader["nomor"];
		$i++;
		$edit["field"][$i]["form_group_class"] = "hiding";
		$edit["field"][$i]["label"] = "Table Name";
		$edit["field"][$i]["input"] = "namatable";
		$edit["field"][$i]["input_attr"]["maxlength"] = "200";
		$edit["field"][$i]["input_attr"]["readonly"] = "readonly";
		if(mysqli_num_rows($resultHeader) > 0)
			$edit["field"][$i]["input_attr"]["value"] = $tableHeader;
		$i++;
		$edit["field"][$i]["form_group_class"] = "hiding";
		$edit["field"][$i]["label"] = "Directory";
		$edit["field"][$i]["input"] = "directory";
		$edit["field"][$i]["input_attr"]["maxlength"] = "200";
		$edit["field"][$i]["input_attr"]["readonly"] = "readonly";
		$edit["field"][$i]["input_attr"]["value"] = "quotation";
		$i++;
		$edit["field"][$i]["form_group_class"] = "hiding";
		$edit["field"][$i]["label"] = "Counter";
		$edit["field"][$i]["input"] = "counter";
		$edit["field"][$i]["input_save"] = "skip";
		$edit["field"][$i]["input_attr"]["readonly"] = "readonly";
		$edit["field"][$i]["input_attr"]["value"] = "1";
		$i++;
		$edit["field"][$i]["label"] = " ";

		$locationprint = "'pages/print_invoice.php?m=".$_SESSION["menu_".$_SESSION["g.menu"]]["string"]."&f=header_grid&&sm=edit&a=view&tipe=bl&no=".$_GET["no"]."','_newtab'";
		$edit["field"][$i]["input_element"] = '<a class="btn btn-default btn-xs btn_print" style="width:350px;" onclick="tambahUploadFile()">&nbsp;&nbsp;Add Upload File&nbsp;&nbsp;</a>';

		$edit["field"][$i]["input_save"] = "skip";
		$i++;
		$edit["field"][$i]["anti_mode"] = "view";
		$edit["field"][$i]["label"] = "Upload File";
		$edit["field"][$i]["input"] = "file_upload_0";
		$edit["field"][$i]["input_attr"]["type"] = "file";
		$edit["field"][$i]["input_attr"]["accept"] = "*";
		$edit["field"][$i]["input_save"] = "skip";
		$i++;
		$edit["field"][$i]["label"] = "Remark";
		$edit["field"][$i]["input"] = "keterangan";
		$edit["field"][$i]["input_element"] = "textarea";
		$edit["field"][$i]["input_attr"]["rows"] = "3";
		$edit["field"][$i]["input_attr"]["cols"] = "44";
		$i++;
		$edit["field"][$i]["anti_mode"] = "add";
		$edit["field"][$i]["label"] = "Status";
		$edit["field"][$i]["input"] = "status_aktif";
		$edit["field"][$i]["input_element"] = "select";
		$edit["field"][$i]["input_option"] = generate_status_option($edit["mode"]);
		$i++;

		if($edit["mode"] != "add")
		{
			$queryData = "SELECT a.* FROM ".$edit["table"]." a WHERE a.nomor = ".$_GET["no"];
			$r = mysqli_fetch_array(mysqli_query($con, $queryData));	
			$edit = fill_value($edit,$r);
		}
		$edit = generate_info($edit,$r,"insert|update");
		$features = "save|back|edit";
		$edit_navbutton = generate_navbutton($edit_navbutton,$features);
		?>

		<script type="text/javascript">
			$(document).ready(function(){
				/*setTimeout(function(){
				  	load_grid_detail();
				}, 10000);*/
				$("#counter").val("1");
			});

			var counter = 1;
			function tambahUploadFile(){
				var counter = $("#counter").val();
				var uploadfilebaru = '<input type="file" accept="*" class="form-control" id="file_upload_'+ counter +'" name="file_upload_'+ counter +'">';
				$( "div[name='div_I_10']" ).append( uploadfilebaru );
				counter++;
				$("#counter").val( counter );
			}
		</script>


<?php } ?>