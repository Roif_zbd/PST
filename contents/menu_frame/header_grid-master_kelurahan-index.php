<?php
$index["debug"] = 1;
$index["ajax"] 	= 1;

 
$i = 0;
$index_table["column"][$i]["name"] 		= "no";
$index_table["column"][$i]["sort"] 		= "empty";
$index_table["column"][$i]["search"] 	= 0;
$index_table["column"][$i]["caption"] 	= "";
$index_table["column"][$i]["align"] 	= "";
$index_table["column"][$i]["width"] 	= "";
$i++;
// $index_table["column"][$i]["name"] 		= "kode";
// $index_table["column"][$i]["sort"] 		= "a.kode";
// $index_table["column"][$i]["search"] 	= 1;
// $index_table["column"][$i]["caption"] 	= "Kode";
// $index_table["column"][$i]["align"] 	= "";
// $index_table["column"][$i]["width"] 	= "";
// $i++;
$index_table["column"][$i]["name"] 		= "nama_kelurahan";
$index_table["column"][$i]["sort"] 		= "a.nama_kelurahan";
$index_table["column"][$i]["search"] 	= 1;
$index_table["column"][$i]["caption"] 	= "Nama Kelurahan";
$index_table["column"][$i]["align"] 	= "";
$index_table["column"][$i]["width"] 	= "";
$i++;
$index_table["column"][$i]["name"] 		= "nama_kecamatan";
$index_table["column"][$i]["sort"] 		= "b.nama_kecamatan";
$index_table["column"][$i]["search"] 	= 1;
$index_table["column"][$i]["caption"] 	= "Nama Kecamatan";
$index_table["column"][$i]["align"] 	= "";
$index_table["column"][$i]["width"] 	= "";
$i++;
$index_table["column"][$i]["name"] 		= "nama_kota";
$index_table["column"][$i]["sort"] 		= "c.nama_kota";
$index_table["column"][$i]["search"] 	= 1;
$index_table["column"][$i]["caption"] 	= "Nama Kota";
$index_table["column"][$i]["align"] 	= "";
$index_table["column"][$i]["width"] 	= "";
$i++;
$index_table["column"][$i]["name"] 		= "nama_provinsi";
$index_table["column"][$i]["sort"] 		= "d.nama_provinsi";
$index_table["column"][$i]["search"] 	= 1;
$index_table["column"][$i]["caption"] 	= "Provinsi";
$index_table["column"][$i]["align"] 	= "";
$index_table["column"][$i]["width"] 	= "";
$i++;
$index_table["column"][$i]["name"] 		= "status_aktif";
$index_table["column"][$i]["sort"] 		= "a.status_aktif";
$index_table["column"][$i]["search"] 	= 0;
$index_table["column"][$i]["caption"] 	= "Status";
$index_table["column"][$i]["align"] 	= "";
$index_table["column"][$i]["width"] 	= "";
$i++;
$index_table["column"][$i]["name"] 		= "action";
$index_table["column"][$i]["sort"] 		= "empty";
$index_table["column"][$i]["search"] 	= 0;
$index_table["column"][$i]["caption"] 	= "Action";
$index_table["column"][$i]["align"] 	= "";
$index_table["column"][$i]["width"] 	= "";
$i++;


$index["query_select"] = "	select a.*,b.nama_kecamatan,c.nama_kota,d.nama_provinsi
							from ".$index["query_from"]." as a
							join mhkecamatan as b 
							on a.nomormhkecamatan = b.nomor
							join mhkota as c
							on b.nomormhkota = c.nomor
							join mhprovinsi as d 
							on c.nomormhprovinsi = d.nomor";
$index["query_where"] .= "	AND a.nomor <> 0";
