<?php
$index_table["data"][$i]["no"] = $no+$index_page["position"]+1;
$index_table["data"][$i]["kode"] = $data["kode"];
$index_table["data"][$i]["usaha"] = $data["usaha"];
$index_table["data"][$i]["nama"] = $data["nama"];
$index_table["data"][$i]["status_aktif"] = $_SESSION["setting"]["status_aktif_".$data["status_aktif"]];
// $features = "edit";
// $index_table["data"][$i]["action"] = generate_navbutton($index_table["data"][$i]["action"],$features,"index",$data["nomor"]);

if($_SESSION["cabang"]["nomor"]==$data["nomor"]){
	$index_table["data"][$i]["action"] .= "<center><a class='btn btn-info btn-app-sm'><i class='fa fa-home' title='Home'></i></a></center>";  
}
else if ($data["is_akses"]==1){
	$index_table["data"][$i]["action"] .= "<center><a class='btn btn-primary btn-app-sm'><i class='fa fa-arrow-right' onclick='confirm(".$data["nomor"].")' title='Go'></i></a></center>";  
}
else{
	$index_table["data"][$i]["action"] .= "<center><a class='btn btn-danger btn-app-sm'><i class='fa fa-ban' title='No Access'></i></a></center>";  
}
?>

<script type="text/javascript">
function confirm(nomor){   
      $.confirm({
          icon: 'fa fa-exchange',
          theme: 'modern',
          closeIcon: true,
          animation: 'scale',
          type: 'blue',
          title: 'NOTIFICATION',
      content: 'Apakah Anda Ingin Pindah Cabang?',
          buttons: {
               Ya:function(){
                    // event.preventDefault();
                    window.open(
                        'dashboard.php?m=cabang_aktif&f=header_grid&no='+nomor,'_parent'
                      );
                                            
                },
                Tidak:function(){
                    event.preventDefault();
                    $.alert({
                      title: 'Decision',
                      content: '<font style="font-size: 14px;">Transaksi Dibatalkan</font>',
                      icon: 'fa fa-warning',
                      type: 'red',
                      
                  });
                },

          }


     });
}
</script>