<?php
$index["debug"] = 0;
$index_navbutton["generate"] = "reload";
$index["filter"] = 1;
$index_filter["hide"] = 0;

$i = 0;
$index_filter["field"][$i]["label"] ="PT";
$index_filter["field"][$i]["input"] ="nomormhusaha";
$index_filter["field"][$i]["input_element"] ="browse";
$index_filter["field"][$i]["browse_setting"] ="master_cabang_pt";
$i++;
$index_filter["field"][$i]["form_group"] =0;
$index_filter["field"][$i]["label"] ="Cabang Toko";
$index_filter["field"][$i]["input"] ="nomormhcabangtoko";
$index_filter["field"][$i]["input_element"] ="browse";
$index_filter["field"][$i]["browse_setting"] ="master_cabang_toko_aset";
$index_filter["field"][$i]["browse_set"]["param_input"]  = array(
	"a.nomormhusaha|browse_master_cabang_pt_hidden"
);
$i++;

if(isset($_GET["no"]))
{
	$cabang = mysqli_fetch_array(mysqli_query($con, "
	SELECT a.*
	FROM mhcabang a
	WHERE a.nomor = ".$_GET["no"]));
	$_SESSION["cabang"]["nomor"] = $cabang["nomor"];
	$_SESSION["cabang"]["kode"] = $cabang["kode"];
	$_SESSION["cabang"]["nama"] = $cabang["nama"];
	$_SESSION["cabang"]["pusat"] = $cabang["pusat"];
/*	if($cabang["pusat"] == 1)
		$_SESSION["cabang"]["restriction"] = "pusat";
	else
		$_SESSION["cabang"]["restriction"] = "cabang";
*/	die("<meta http-equiv='refresh' content='0;URL=dashboard.php'>");
}

$i = 0;
$index_table["column"][$i]["name"] = "no";
$index_table["column"][$i]["sort"] = "empty";
$index_table["column"][$i]["search"] = 0;
$index_table["column"][$i]["caption"] = "";
$index_table["column"][$i]["align"] = "";
$index_table["column"][$i]["width"] = "";
$i++;
$index_table["column"][$i]["name"] = "kode";
$index_table["column"][$i]["sort"] = "a.kode";
$index_table["column"][$i]["search"] = 1;
$index_table["column"][$i]["caption"] = "Kode Cabang";
$index_table["column"][$i]["align"] = "";
$index_table["column"][$i]["width"] = "150";
$i++;
$index_table["column"][$i]["name"] = "nama";
$index_table["column"][$i]["sort"] = "a.nama";
$index_table["column"][$i]["search"] = 1;
$index_table["column"][$i]["caption"] = "Nama Cabang";
$index_table["column"][$i]["align"] = "";
$index_table["column"][$i]["width"] = "300";
$i++;
$index_table["column"][$i]["name"] = "usaha";
$index_table["column"][$i]["sort"] = "b.nama";
$index_table["column"][$i]["search"] = 1;
$index_table["column"][$i]["caption"] = "PT";
$index_table["column"][$i]["align"] = "";
$index_table["column"][$i]["width"] = "300";
$i++;
$index_table["column"][$i]["name"] = "status_aktif";
$index_table["column"][$i]["sort"] = "a.status_aktif";
$index_table["column"][$i]["search"] = 0;
$index_table["column"][$i]["caption"] = "Status";
$index_table["column"][$i]["align"] = "";
$index_table["column"][$i]["width"] = "";
$i++;
$index_table["column"][$i]["name"] = "action";
$index_table["column"][$i]["sort"] = "empty";
$index_table["column"][$i]["search"] = 0;
$index_table["column"][$i]["caption"] = "Action";
$index_table["column"][$i]["align"] = "50";
$index_table["column"][$i]["width"] = "";
$i++;

$index["query_select"] = "	SELECT a.*, b.nama as usaha,
							IF(
								".$_SESSION["login"]["tingkatan"]."<=1 OR a.nomor IN (
									SELECT nomor
									FROM shaksesmaster
									WHERE
										status_aktif=1 
										AND nomormhadmin=".$_SESSION["login"]["nomor"]."
										AND relasi_tipe='master_cabang'
										AND relasi_table='mhcabang'
								),1,0
							) as is_akses
							FROM ".$index["query_from"]." a
							JOIN mhusaha b ON a.nomormhusaha = b.nomor
							";
$index["query_where"] .= "	
							AND a.nomormhusaha like '".$_SESSION["usaha"]["nomor"]."'
							AND a.nomor <> 0 AND a.status_aktif > 0 ";
$index["default_order"] = "	a.urutan";
?>