<?php
$acomplete["query"] = "
	SELECT a.*,
	bst.nama AS satuan,
	bbk.nama AS kategori,
	CONCAT('[', a.kode, '] ', ' - ',a.nama) info,
	0 AS harga
	FROM mhbarang a
	JOIN mhsatuan bst ON a.nomormhsatuan = bst.nomor AND bst.status_aktif > 0
	JOIN mhbarangkategori bbk ON a.nomormhbarangkategori = bbk.nomor AND bbk.status_aktif > 0
	WHERE a.status_aktif = 1 AND
	a.nomormhusaha LIKE '".$_SESSION["usaha"]["nomor"]."'
	? ";
$acomplete["query_order"] = "a.kode";
$acomplete["query_search"] = array("a.kode","a.nama","CONCAT('[', a.kode, '] ', ' - ',a.nama)");
$acomplete["items"] = array(
	"nomor",
	"kode",
	"nama",
	"harga_jual",
	"info",
	"nomormhsatuan",
	"satuan",
	"harga",
	"kategori"
);
$acomplete["items_visible"] = array("info");
$acomplete["items_selected"] = array("info");
$acomplete["param_input"] = array();
$acomplete["debug"] = 1;
?>