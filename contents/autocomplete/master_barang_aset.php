<?php
$acomplete["query"] = "
	SELECT a.*,
	d.nomor as nomormhsatuan,
	d.nama AS nama_nomormhsatuan,
	c.jumlah as jumlah_konversi
	FROM mhbarangaset a
	JOIN mdbarangasetsatuan c ON c.nomormhbarangaset=a.nomor AND c.status_aktif=1 AND c.terkecil=1
	JOIN mhsatuan d ON d.nomor=c.nomormhsatuan
	WHERE a.status_aktif = 1 ? ";
$acomplete["query_order"] = "a.kode";
$acomplete["query_search"] = array("a.kode","a.nama");
$acomplete["items"] = array(
	"nomor",
	"kode",
	"nama",
	"nama_nomormhsatuan",
	"nomormhsatuan",
	"jumlah_konversi"
);
$acomplete["items_visible"] = array("kode","nama");
$acomplete["items_selected"] = array("kode","nama");
$acomplete["param_input"] = array();
$acomplete["debug"] = 1;
?>