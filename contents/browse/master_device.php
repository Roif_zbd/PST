<?php
$browse["id"] 				= "master_device";
$browse["caption"] 			= "Browse Device";
$browse["query"] 			= "	SELECT a.*, CONCAT('[', a.kode, '] ', a.nama) AS info 
								FROM mhdevice a
								WHERE a.status_aktif = 1 ? ";
$browse["query_order"] 		= " a.kode";
$browse["query_search"] 	= array("a.nama","a.kode");
$browse["param_input"] 		= array();
$browse["param_output"] 	= array();
$browse["items"] 			= array("nomor||true", "kode", "nama", "info");
$browse["items_visible"] 	= array("info");
$browse["items_selected"] 	= array("info");
$browse["selected_url"] 	= "?m=master_device_data&f=header_grid&sm=edit&a=view&no=";
$browse["new_url"] 			= "?m=master_device_data&f=header_grid&sm=edit";
$browse["autocomplete_url"] = "";
$browse["grid"] 			= "";
$browse["grid_editing"] 	= "";
$browse["grid_val"] 		= "";
$browse["grid_values"] 		= array();
$browse["call_function"] 	= "";
$browse["custom_function"] 	= "";
$browse["debug"] 			= 1;
?>