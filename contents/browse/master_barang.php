<?php
$browse["id"] 				= "master_barang";
$browse["caption"] 			= "Browse Barang";
$browse["query"] 			= "	
								SELECT a.*,
								bst.nama AS satuan,
								bbk.nama AS kategori,
								CONCAT('[', a.kode, '] ', ' - ',a.nama) info,
								0 AS harga
								FROM mhbarang a
								JOIN mhsatuan bst ON a.nomormhsatuan = bst.nomor AND bst.status_aktif = 1
								JOIN mhbarangkategori bbk ON a.nomormhbarangkategori = bbk.nomor AND bbk.status_aktif = 1
								WHERE a.status_aktif = 1 AND
								a.nomormhusaha LIKE '".$_SESSION["usaha"]["nomor"]."'
								? ";
$browse["query_order"] 		= " a.kode";
$browse["query_search"] 	= array("a.kode", 
									"CONCAT('[', a.kode, '] ', ' - ',a.nama)"
									);
$browse["param_input"] 		= array();
$browse["param_output"] 	= array();
$browse["items"] 			= array("nomor||true", "info", "kategori", "satuan");
$browse["items_visible"] 	= array("info");
$browse["items_selected"] 	= array("info");
$browse["selected_url"] 	= "?m=master_barang_data&f=header_grid&sm=edit&a=view&no=";
$browse["new_url"] 			= "?m=master_barang_data&f=header_grid&sm=edit";
$browse["autocomplete_url"] = "";
$browse["grid"] 			= "";
$browse["grid_editing"] 	= "";
$browse["grid_val"] 		= "";
$browse["grid_values"] 		= array();
$browse["call_function"] 	= "";
$browse["custom_function"] 	= "";
$browse["debug"] 			= 1;
