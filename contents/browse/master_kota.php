<?php
// $browse["id"] 					= "master_kota";
// $browse["caption"] 				= "Browse Kota";
// $browse["query"] 				= "SELECT a.*, b.nama_provinsi, 
// 									CONCAT('[', a.nama_kota, '] ') as info
// 													FROM mhkota as a
// 									JOIN mhprovinsi as b
// 									ON a.nomormhprovinsi = b.nomor
// 									WHERE 
// 									a.status_aktif = 1 ?
// 									";
// $browse["query_order"] 			= "a.nama_kota"; 
// $browse["query_search"] 		= array("a.nama_kota","CONCAT('[', a.nama_kota, '] ')");
// $browse["param_input"] 			= array();
// $browse["param_output"] 		= array("info");
// $browse["items"] 				= array("nomor||true","kode|Kode", "nama_provinsi|Provinsi","info");
// $browse["items_visible"] 		= array("info");
// $browse["items_selected"] 		= array("info");
// $browse["selected_url"] 		= "";
// $browse["new_url"] 				= "";
// $browse["autocomplete_url"] 	= "";
// $browse["grid"] 				= "";
// $browse["grid_editing"] 		= "";
// $browse["grid_val"] 			= "";
// $browse["grid_values"] 			= array();
// $browse["call_function"] 		= "";
// $browse["custom_function"] 		= "";
// $browse["debug"] 				= 1;
// $browse["selected_mode"] 		= "off";

$browse["id"] 					= "master_kota";
$browse["caption"] 				= "Browse Kota";
$browse["query"] 				= "SELECT a.*,b.nama_provinsi
									FROM mhkota as a
									JOIN mhprovinsi as b
									ON a.nomormhprovinsi = b.nomor
									WHERE 
									a.status_aktif = 1 ?
									";
$browse["query_order"] 			= "a.nama_kota"; 
$browse["query_search"] 		= array("a.nama_kota");
$browse["param_input"] 			= array();
$browse["param_output"] 		= array();
$browse["items"] 				= array("nomor||true","nama_kota|Kota", "nama_provinsi|Provinsi");
$browse["items_visible"] 		= array("nama_kota");
$browse["items_selected"] 		= array("nama_kota");
$browse["selected_url"] 		= "";
$browse["new_url"] 				= "";
$browse["autocomplete_url"] 	= "";
$browse["grid"] 				= "";
$browse["grid_editing"] 		= "";
$browse["grid_val"] 			= "";
$browse["grid_values"] 			= array();
$browse["call_function"] 		= "";
$browse["custom_function"] 		= "";
$browse["debug"] 				= 1;
$browse["selected_mode"] 		= "off";
?>