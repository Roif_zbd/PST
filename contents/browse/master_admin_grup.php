<?php
$browse["id"] = "master_admin_grup";
$browse["caption"] = "Browse Grup Admin";
$browse["query"] = "SELECT a.*
					FROM mhadmingrup a
					WHERE a.status_aktif = 1 ?";
$browse["query_order"] = "a.nama";
$browse["query_search"] = array("a.kode","a.nama");
$browse["param_input"] = array();
$browse["param_output"] = array();
$browse["items"] = array("nomor||true","kode","nama|Grup Admin");
$browse["items_visible"] = array("kode","nama");
$browse["items_selected"] = array("kode","nama");
$browse["selected_url"] = "?m=master_admin_grup&f=header_grid&sm=edit&a=view&no=";
// $browse["new_url"] = "?m=master_admin_grup&f=header_grid&sm=edit";
$browse["autocomplete_url"] = "";
$browse["grid"] = "";
$browse["grid_editing"] = "";
$browse["grid_val"] = "";
$browse["grid_values"] = array();
$browse["call_function"] = "";
$browse["custom_function"] = "";
$browse["debug"] = 1;
$browse["selected_mode"] = "off";
?>