<?php
$browse["id"] 				= "master_gudang";
$browse["caption"] 			= "Browse Gudang";
$browse["query"] 			= "	SELECT a.*, CONCAT('[', a.kode, '] ', b.nama, ' - ', a.nama) AS info, b.nomormhusaha
								FROM mhgudang a
								JOIN mhcabang b ON b.nomor = a.nomormhcabang
								-- JOIN shaksesmaster bgd ON bgd.relasi_nomor = a.nomor AND bgd.status_aktif > 0 AND bgd.relasi_tabel = 'mhgudang'
								WHERE 
									a.status_aktif = 1
									AND a.nomormhcabang = ".$_SESSION["cabang"]["nomor"]."
									-- AND bgd.nomormhusaha = ".$_SESSION["usaha"]["nomor"]." 	
									-- AND bgd.nomormhadmin = ".$_SESSION["login"]["nomor"]."
								? ";
$browse["query_order"] 		= " a.nomor";
$browse["query_search"] 	= array("a.nama", "a.kode", "b.nama");
$browse["param_input"] 		= array();
$browse["param_output"] 	= array();
$browse["items"] 			= array("nomor||true", "kode", "nama", "info||true", "nomormhusaha||true");
$browse["items_visible"] 	= array("info");
$browse["items_selected"] 	= array("info");
$browse["selected_url"] 	= "?m=master_gudang_data&f=header_grid&sm=edit&a=view&no=";
$browse["new_url"] 			= "?m=master_gudang_data&f=header_grid&sm=edit";
$browse["autocomplete_url"] = "";
$browse["grid"] 			= "";
$browse["grid_editing"] 	= "";
$browse["grid_val"] 		= "";
$browse["grid_values"] 		= array();
$browse["call_function"] 	= "";
$browse["custom_function"] 	= "";
$browse["debug"] 			= 1;
?>