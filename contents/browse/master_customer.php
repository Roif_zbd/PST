<?php
$browse["id"] 				= "master_customer";
$browse["caption"] 			= "Browse Customer";
$browse["query"] 			= "	SELECT a.*, CONCAT('[', a.kode, '] ', a.nama) AS info ,
								DATE_FORMAT(DATE_ADD(CURDATE(), INTERVAL 1 DAY),'%d/%m/%Y') tgl
								FROM mhrelasi a
								WHERE a.status_aktif = 1 
								AND FIND_IN_SET(a.`nomormhrelasijenis`, 2)
								AND a.nomormhusaha LIKE '".$_SESSION["usaha"]["nomor"]."'
								? ";
$browse["query_order"] 		= " a.kode";
$browse["query_search"] 	= array("a.nama","a.kode", "CONCAT('[', a.kode, '] ', a.nama)");
$browse["param_input"] 		= array();
$browse["param_output"] 	= array();
$browse["items"] 			= array("nomor||true", "kode", "nama", "info||true","top||true", "tgl||true", "pph||true");
$browse["items_visible"] 	= array("info");
$browse["items_selected"] 	= array("info");
$browse["selected_url"] 	= "?m=master_relasi&f=header_grid&sm=edit&a=view&no=";
$browse["new_url"] 			= "?m=master_relasi&f=header_grid&sm=edit";
$browse["autocomplete_url"] = "";
$browse["grid"] 			= "";
$browse["grid_editing"] 	= "";
$browse["grid_val"] 		= "";
$browse["grid_values"] 		= array();
$browse["call_function"] 	= "";
$browse["custom_function"] 	= "";
$browse["debug"] 			= 1;
?>