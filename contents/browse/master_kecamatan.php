<?php
$browse["id"] 					= "master_kecamatan";
$browse["caption"] 				= "Browse Kecamatann";
	// select a.*,b.*,c.*,d.*
    // from ".$index["query_from"]." as a
    // join mhkecamatan as b 
    // on a.nomormhkecamatan = b.nomor
    // join mhkota as c
    // on b.nomormhkota = c.nomor
    // join mhprovinsi as d 
    // on c.nomormhprovinsi = d.nomor";

	// SELECT a.*, b.nama_provinsi, 
// 	CONCAT('[', a.nama_kota, '] ') as info
// 	FROM mhkota as a
// JOIN mhprovinsi as b
// ON a.nomormhprovinsi = b.nomor

$browse["query"] 				= "
								select a.*,a.nama_kecamatan as info1, b.nama_kota as info2, c.nama_provinsi as info3
								from mhkecamatan as a
								join mhkota as b 
								on a.nomormhkota = b.nomor
								join mhprovinsi as c
								on b.nomormhprovinsi = c.nomor
	
					
								WHERE 
								a.status_aktif = 1 ?
									";
$browse["query_order"] 			= "a.nama_kecamatan"; 
$browse["query_search"] 		= array("a.nama_kecamatan");
$browse["param_input"] 			= array();
$browse["param_output"] 		= array();
$browse["items"] 				= array("nomor||true","info1|Kecamatan","info2|Kelurahan","info3|Provinsi");
$browse["items_visible"] 		= array("info1");
$browse["items_selected"] 		= array("info1");
$browse["selected_url"] 		= "";
$browse["new_url"] 				= "";
$browse["autocomplete_url"] 	= "";
$browse["grid"] 				= "";
$browse["grid_editing"] 		= "";
$browse["grid_val"] 			= "";
$browse["grid_values"] 			= array();
$browse["call_function"] 		= "";
$browse["custom_function"] 		= "";
$browse["debug"] 				= 1;
$browse["selected_mode"] 		= "off";
?>