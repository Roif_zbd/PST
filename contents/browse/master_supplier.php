<?php
$browse["id"] 					= "master_supplier";
$browse["caption"] 				= "Browse Supplier";
$browse["query"] 				= "SELECT a.*, 
                          CONCAT('[', a.nama, '] ') as info
								    FROM mhsupplier as a
									WHERE 
									a.status_aktif = 1 ?
									";
$browse["query_order"] 			= "a.nama"; 
$browse["query_search"] 		= array("a.nama","CONCAT('[', a.nama, '] ')");
$browse["param_input"] 			= array();
$browse["param_output"] 		= array("info");
$browse["items"] 				= array("nomor||true","info");
$browse["items_visible"] 		= array("info");
$browse["items_selected"] 		= array("info");
$browse["selected_url"] 		= "";
$browse["new_url"] 				= "";
$browse["autocomplete_url"] 	= "";
$browse["grid"] 				= "";
$browse["grid_editing"] 		= "";
$browse["grid_val"] 			= "";
$browse["grid_values"] 			= array();
$browse["call_function"] 		= "";
$browse["custom_function"] 		= "";
$browse["debug"] 				= 1;
$browse["selected_mode"] 		= "off";
?>