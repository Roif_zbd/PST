<?php
$browse["id"] 					= "master_admin";
$browse["caption"] 				= "Browse Admin";
$browse["query"] 				= "SELECT DISTINCT a.*, c.nomormhusaha nomormhusaharelasi, b.nama grup,
   									CONCAT('[', a.kode, '] ', a.nama) AS info
									FROM shaksesmaster c
									join mhadmin a ON c.nomormhadmin = a.nomor
									JOIN mhadmingrup b ON c.`relasi_nomor`= b.nomor AND c.`relasi_tipe` = 'master_group' AND b.status_aktif > 0 AND c.`relasi_nomor` = 3
									WHERE c.status_aktif = 1
									#AND b.tingkatan >=1 
									?";
$browse["query_order"] 			= "a.nama";
$browse["query_search"] 		= array("a.kode","a.nama","CONCAT('[', a.kode, '] ', a.nama)");
$browse["param_input"] 			= array();
$browse["param_output"] 		= array();
$browse["items"] 				= array("nomor||true","kode","nama","info","grup|Grup Admin", "nomormhusaharelasi||true","email||true","telepon||true");
$browse["items_visible"] 		= array("info","grup");
$browse["items_selected"] 		= array("info","grup");
$browse["selected_url"] 		= "";
$browse["new_url"] 				= "";
$browse["autocomplete_url"] 	= "";
$browse["grid"] 				= "";
$browse["grid_editing"] 		= "";
$browse["grid_val"] 			= "";
$browse["grid_values"] 			= array();
$browse["call_function"] 		= "";
$browse["custom_function"] 		= "";
$browse["debug"] 				= 1;
$browse["selected_mode"] 		= "off";
?>